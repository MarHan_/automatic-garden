#include <LiquidCrystal.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>



/*
###########################################################################################
#    |                                                                                    #
#  .'|'.                        CODE DU PROJET D'ISN                                      #
# /.'|\ \          PARTIE CAPTEURS ET ACTIONS : MARIN ZAOUIT-QUENCEZ                      #
# | /|'.|                PATIE AFFICHAGE : ORGYEN BOINVILLIERS                            #
#  \ |\/                                                                                  #
#   \|/                                                                                   #
#    `                                                                                    #
###########################################################################################
 */

/*
###########################################################################################
#                    Déclaration des variables et constantes du système                   #
###########################################################################################
 */ 

// Constantes de référence et de branchements :
const int lightSensor = A0;         // Port auquel est assigné le capteur de luminosité
const int lightPin = 13;
const int valvePin = 4;          // Port auquel est assigné le la valve
const int alarmePin = 5;         // Port auquel est assigné le buzzer
const int tankPin = 20;          // Port auquel est assigné le détecteur de fin de réservoir
const int up = 8;                // On déclare le bouton haut/oui 
const int sel = 9;               // Bouton select
const int down = 10;             // Bouton bas

// Variables du Système :
int humidite = 0;                // Valeur de l'humidité en pourcentage
float temperature = 0;           // Valeur de la température en degrés Celcius
float lightAnalog = 0;           // Valeur de la luminosité en voltage de O à 3.3V
int light = 0;                   // Valeur de la luminosité en pourcentage
int lightPower = 0;
bool tankFin = false;

// Variables de Réglages :
int humiditeReg = 20;            // Valeur de réglage de l'humidité minimum
int temperatureReg = 5;          // Valeur de réglage de la température minimum
int lightReg = 70;               // Valeur de réglage de la luminosité minimum

//Compteurs permettant de changer de pages et de sub-menus

int compteur_page = 1 ;       // Cycler entre les pages
int compteursub_page = 0;     // Modifier le sub-menu humidité
int subpage2_compteur = 0;    // Pour aller au sous-menu led


//---------Etat des boutons pour la fonction debounce-----//
boolean current_up = LOW;          
boolean last_up = LOW;            
boolean current_sel = LOW;
boolean last_sel = LOW;
boolean last_down = LOW;
boolean current_down = LOW;

// Charactère pour dire où  exit le sub-menu
byte back[8] = {
  0b00100,
  0b01000,
  0b11111,
  0b01001,
  0b00101,
  0b00001,
  0b00001,
  0b11111
};

// Charactère pour savoir où est la flèche select
byte arrow[8] = {
  0b01000,
  0b00100,
  0b00010,
  0b11111,
  0b00010,
  0b00100,
  0b01000,
  0b00000
};

#define DHTPIN 6
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

LiquidCrystal lcd(2, 3, 4, 5, 6, 7); //On déclare les pins

/*
###########################################################################################
#                               Mise en place du système                                  #
###########################################################################################
 */

void setup() {
// Initialisation de nos ports logiques
pinMode(alarmePin, OUTPUT); 
pinMode(valvePin, OUTPUT);
digitalWrite(valvePin, LOW);

lcd.begin(16,2);
lcd.createChar(1, back);
lcd.createChar(2, arrow);
lcd.setCursor(0,0);

analogReference(EXTERNAL);       // Les lectures analogique ont voltage max de 3.3V

lcd.print("Booting...");
delay(5000);
lcd.clear();  

}

void alarme(){
  for (int i=0; i <= 5; i++){
    tone(alarmePin, 500, 50);    // Utilisation du buzzer à une fréquence de 500Hz
    delay(50);                   // Attente avant un signal supplémentaire durant 50ms
    tone(alarmePin, 600, 50);    // Utilisation du buzzer à une fréquence de 600Hz
    delay(1000);                 // Attente avant un signal supplémentaire durant 1s
  }
}


/*
###########################################################################################
#                 Déclaration des fonctions de mesure par les capteurs                    #
###########################################################################################
 */

// Mesure de l'Humidité
int mesureHumidite() {
  humidite = dht.readHumidity();                  // Lecture de la valeur d'humidité dans le DHT
}

// Mesure de la température
int mesureTemperature() {
  temperature = dht.readTemperature();            // Lecture de la valeur de température dans le DHT
}

// Mesure de la luminosité
int mesureLight() {
  lightAnalog = analogRead(lightPin);             // Lecture analogique du capteur de luminosité
  light = map (lightAnalog, 0, 3.3, 0, 100);      // Conversion de la valeur en pourcentage
}   

// Vérification de la présence d'eau dans le réservoir du système
bool tankVerif() {
  if ( digitalRead(tankPin) == 0 ){ // Si le détecteur de fin de réservoir retourne un valeur négative
    tankFin = true;                // Le réservoir est défini comme étant vide
  }
}


/*
###########################################################################################
#                          Déclaration des fonctions d'action                             #
###########################################################################################
 */

void arrosage() {
mesureHumidite();                     // Mesure de l'humidité 
while ( humidite < humiditeReg ){     // Tant que l'humidité actuelle ne satisfait pas le réglage de base :
  digitalWrite(valvePin,HIGH);        // Ouverture de la vanne pendant une durée de 2s
  delay(2000);
  digitalWrite(valvePin,LOW);
  delay(5000);                        // Attente de la propagation de l'eau dans la terre avant de prendre une nouvelle mesure
  void mesureHumidite();              // Nouvelle mesure afin de vérifier si la nouvelle valeur d'humidité correspond désormais au réglage
  }
}

void eclairage() {
while ( lightPower< 200 ) {           // Tant que la luminosité actuelle ne satisfait pas le réglage de base :
  analogWrite(lightPin, lightPower);  // Initialisation de l'éclairage à une intensité spécifique
  lightPower = lightPower+5;          // Augmentation de cette intensité
  delay(50);                          // Micro délai de sécurité
  }
mesureLight();                        // Mesure de la luminosité
}

// Note : L'initialisation de l'éclairage se fait par incrémentation uniquement par soucis d'esthétisme.

void temperatureLimite(){             // On créé une fonction de vérification de la température par sécurité afin d'alerter l'utilisateur en cas de problème 
mesureTemperature();                  // Mesure de la température
  if (temperature < temperatureReg){  // Si la température actuelle ne satisfait pas le réglage de base
    void alarme();                    // Alerte de l'utilisateur par le biais de l'alarme
  }
}

// Note : La température pourrait être modifiée automatiquement plutôt que par l'utilisateur avec l'implant d'un module thermique dans le système.

//---- Fonction debounce pour tout les boutons (debounce = vérifier si le bouton est appuyé)----//
   
boolean debounce(boolean last, int pin)
{
boolean current = digitalRead(pin);
if (last != current)
{
delay(5);
current = digitalRead(pin);
}
return current;
}

void menu() {
current_up = debounce(last_up, up);         //Debounce le bouton Up
current_sel = debounce(last_sel, sel);      //Debounce le bouton Select
current_down = debounce(last_down, down);   //Debounce le bouton Down

//----Page compteur function to move pages----//

if(compteursub_page==0 && subpage2_compteur==0){                 // Les boutons up/down fonctionnent si le compteursubs est 0. Ils ne marchent pas si c'est 1,2 etc.. Pour travailler dans les submenus
  
//Monter d'une page
    if (last_up== LOW && current_up == HIGH){ // Quand le bouton up est appuyé
      lcd.clear();                            //On clear l'ancienne LCD si elle change pour pouvoir écrire sur un nouvel écran
      if(compteur_page <3){              //Le compteur de pages n'est jamais > 3 (total des pages) 
      compteur_page= compteur_page +1;   //On augmente d'une page
      
      }
      else{
      compteur_page= 1;                 //On passe à la page 1 si on dépasse la page 3 (changer la valeur à 3 pour rester bloqué)
      }
  
}

 
last_up = current_up;                 //On sauvegarde le dernier état du bouton up

//Descendre d'une page
    if (last_down== LOW && current_down == HIGH){//Quand le bouton down est appuyé
      lcd.clear();                       // On clear l'ancien  affichage LCD si il change pour pouvoir écrire sur un nouvel écran
      if(compteur_page >1){              // Le compteur de pages n'est jamais <1 car c'est la plus petite
      compteur_page= compteur_page -1;   // On descend d'une page
      
      }
      else{
      compteur_page= 1;                 //On reste bloqué à la page 1 si on descend encore
      }
  }
    
    last_down = current_down;         //On sauvegarde le dernier état du bouton down
}
//------- La fonction switch où l'on va insérer les écrans de notre menu---// 
  switch (compteur_page) {
   
    case 1 : {     //Page 1
      lcd.setCursor(0,0);
      lcd.print("Temperature : ");
      lcd.setCursor(0,1);
      lcd.print(temperature);

    }//Fin Case 1
    break;

    case 2 : { //Page 2 (humidité?)
     lcd.setCursor(0,0);
     lcd.print("Humidity : ");
     lcd.setCursor(0,1);
     lcd.print(humidite);
     lcd.setCursor(10,1);
     lcd.print("Set");
     lcd.print(byte(2));
     
     lcd.setCursor(15,1);
     lcd.write(byte(1));     //A la fin il faut mettre le charactère 1 (exit)

     
      
     //Les fonctions
     
     // Contrôle du compteur des sub-menus
     if (last_sel== LOW && current_sel == HIGH){ // On appuye sur le bouton Select
      if(compteursub_page <2){                    // Le compteur des sub menu < 2 (On modifie que l'humidité)
     compteursub_page ++;                         // On augmente de 1 si le compteur sub <1
     }
     else{                                        // Si le submenu = 2 on revient à la page 1
      compteursub_page=1;
     }
     }
     last_sel=current_sel;                        //On sauvegarde le dernier état du bouton select
     
     //Contrôle de l'humidité
     if(compteursub_page==1){
    

     ///////// INSERER LE REGLAGE DE L'HUMIDITE AVEC LE POTENTIOMETRE ICI//////
  

    
    }
    break;
    
  } //  case2 end

    case 3 : {
      lcd.setCursor(0,0);
      lcd.print("Light : ");
      lcd.setCursor(0,1);
      lcd.print(light);
    }

} // switch end
}

/*
###########################################################################################
#                                   Programme Principal                                   #
###########################################################################################
 */

void loop() {
 //menu();
 eclairage();                  // Exécution de la fonction d'éclairage
 //temperatureLimite();        // Exécution de la fonction de sécurité de température
 // tankVerif();                // Vérification de la présence d'eau dans le réservoir du système
 /* if ( tankFin == true ){   // Tant que le réservoir est vide :
 alarme();                   // Alerte de l'utilisateur
}*/
 //arrosage();             // Exécution de la fonction d'arrosage
}







